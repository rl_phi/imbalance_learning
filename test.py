import numpy as np
from sklearn.svm import *
from math import *
from sklearn.metrics import *
from sklearn.model_selection import train_test_split
from imblearn.over_sampling import *
from imblearn.under_sampling import *
from imblearn.metrics import *
from imblearn.datasets import fetch_datasets
from sklearn.neighbors import *
from sklearn.tree import DecisionTreeClassifier
from random import *
import pandas as pd


def myway(x, y, under_rate, k, w):
	num = numofclasses(y)
	n = len(y)
	important = [[0, random(), i] for i in range(n)]
	nbrs = NearestNeighbors(n_neighbors=k+1, algorithm='ball_tree').fit(x)
	dis, ind = nbrs.kneighbors(x)
	for i in range(n):
		for j in range(1, k+1):
			if y[ind[i][j]] == y[i]:
				important[ind[i][j]][0] += w[y[i]]
			else:
				important[ind[i][j]][0] -= w[y[i]]
	important.sort(reverse=1)
	n_maj = {}
	for maj in under_rate:
		n_maj[maj] = int(under_rate[maj] * num[maj])
	maj_picked = {maj: 0 for maj in n_maj}
	results = []
	for i in important:
		if y[i[2]] in n_maj and maj_picked[y[i[2]]] < n_maj[y[i[2]]]:
			maj_picked[y[i[2]]] = maj_picked[y[i[2]]] + 1
			results.append(i[2])
		if y[i[2]] not in n_maj:
			results.append(i[2])
	return [x[results,:], y[results]]




def get_measures(clf, trainx, trainy, testx, testy):
	clf.fit(trainx, trainy)
	y_pred = clf.predict(testx)
	y_pred_score = clf.predict_proba(testx)[:,1]
	# sensitivity_score(testy, y_pred, average='macro')
	return {'gmean': geometric_mean_score(testy, y_pred)), 'roc_auc_score_macro': roc_auc_score(testy, y_pred_score, average='macro')}


def test_onedata_binary(clf, x, y, name):
	x_train, x_test, y_train, y_test = train_test_split(x, y, random_state=0)
	nums = numofclasses(y_train)
	diffnum = nums[-1] - nums[1]
	change_rate = 1./50
	rates = list(np.arange(0, 1.01, change_rate))
	num_iter = 10
	undersamplings = [['RUS', RandomUnderSampler(), num_iter],
	['NCL', NeighbourhoodCleaningRule(), num_iter],
	['NM2', NearMiss(version=2), num_iter]]

	oversamplings = [['ROS', RandomOverSampler(), num_iter],
	['SMOTE', SMOTE(), num_iter],
	['ADASYN', ADASYN(), num_iter],
	['BLSMOTE', BorderlineSMOTE(), num_iter]]
	results = {}
	measures = get_measures(clf, x_train, y_train, x_test, y_test)
	results['BASE'] = measures
	for j,sampling in enumerate(undersamplings + oversamplings):
		print (sampling[0])
		s = sampling[1]
		best_measures = {i:0 for i in measures}
		for rate in rates:
			if j < len(undersamplings):
				if rate > 0.6:
					continue
				if j!=1:
					s.sampling_strategy = {-1: nums[1] + int(diffnum * rate)}
			else:
				if rate < 0.05:
					continue
				s.sampling_strategy = {1: nums[1] + int(diffnum * rate)}
			average_measures = {i:0 for i in measures}
			for i in range(sampling[2]):
				newx, newy = s.fit_sample(x_train, y_train)
				measures = get_measures(clf, newx, newy, x_test, y_test)
				for i in measures:
					average_measures[i] += measures[i]

			for i in best_measures:
				best_measures[i] = max(best_measures[i], average_measures[i]/sampling[2])

		results[sampling[0]] = best_measures

	print('ncl+smote')
	temp_x, temp_y = NeighbourhoodCleaningRule().fit_sample(x_train, y_train)
	temp_nums = numofclasses(temp_y)
	temp_diff = temp_nums[-1] - temp_nums[1]
	s = SMOTE()
	best_measures = {i:0 for i in measures}
	for rate in rates:
		s.sampling_strategy = {1: temp_nums[1] + int(temp_diff * rate)}
		average_measures = {i:0 for i in measures}
		for i in range(num_iter):
			newx, newy = s.fit_sample(temp_x, temp_y)
			measures = get_measures(clf, newx, newy, x_test, y_test)
			for i in measures:
				average_measures[i] += measures[i]

		for i in best_measures:
			best_measures[i] = max(best_measures[i], average_measures[i]/num_iter)
	results['NCL + SMOTE'] = best_measures

	best_measures = {i:0 for i in measures}
	for rate in list(np.arange(float(nums[1])/nums[-1], 0.6, change_rate)):
		k = min(int(len(x_train)/100), 200)
		wneg, wpos = 1/nums[-1], 1/nums[1]
		average_measures = {i:0 for i in measures}
		for i in range(num_iter):
			newx, newy = myway(x_train, y_train, {-1:rate}, k, {-1:wneg, 1:wpos})
			measures = get_measures(clf, newx, newy, x_test, y_test)
			for i in measures:
				average_measures[i] += measures[i]
		for i in best_measures:
			best_measures[i] = max(best_measures[i], average_measures[i]/num_iter)
	results['NCU'] = best_measures
	return results


def complete_test():
	result_df = pd.DataFrame(columns = ['data', 'measure', 'BASE', 'RUS', 'NCL', 'NM2', 'ROS', 'SMOTE', 'ADASYN', 'BLSMOTE', 'NCL + SMOTE', 'NCU'])
	
	names = ['ecoli', 'sick_euthyroid', 'spectrometer', 'us_crime', 'oil', 'wine_quality', 'yeast_me2', 'mammography', 'optical_digits', 'satimage']
	data = fetch_datasets()
	for name in names[:]:
		print(name, '----------------------------------------\n')
		result = test_onedata_binary(KNeighborsClassifier(), data[name]['data'], data[name]['target'], name)
		dicts = []
		for measure in result['NCU']:
			dicts.append({'data': name, 'measure': measure})
			for alg in result:
				dicts[-1][alg] = result[alg][measure]
		print(dicts)
		for dic in dicts:
			result_df.loc[len(result_df)] = dic

	result_df.to_pickle("knn.pkl")


complete_test()
